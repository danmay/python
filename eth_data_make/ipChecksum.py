# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import matplotlib.pyplot as plt

import pylab


#% % Try program


#t = linspace(1,1,100)

#sin(t)

def quicksort(arr):
    if len(arr) <= 1:
        return arr
    pivot = arr[len(arr) // 2]
    left = [x for x in arr if x < pivot]
    middle = [x for x in arr if x == pivot]
    right = [x for x in arr if x > pivot]
    return quicksort(left) + middle + quicksort(right)

def ipChecksum(headerList,calc):
    print(headerList)
    wortList = []
    checksum = 0
    wordCount = 0
    pow2_16 = pow(2,16)
    for byte in range(0, 20, 2):
        #print(headerList[byte])
        wortList.append(headerList[byte]+headerList[byte+1])
    #print(wortList)

    for word in wortList:
        wordCount += 1
        if (wordCount == 6 and calc == 1):
            word = "0"
        checksum = checksum + int(word,16)
        res = int(checksum / pow2_16)
        #print(res)
        if (res > 0):
            checksum = checksum - res * pow2_16 + res

    #print(hex(checksum))
    checksum = ~checksum & 0xffff
    #print(hex(checksum))

    return checksum



headerList = ["12","34","56","78","9a","bc","dc","de","fe","df","64","d6","56","78","9a","bc","dc","d1","ed","fa"]
print(hex(ipChecksum(headerList,1)))
print(hex(ipChecksum(headerList,0)))
'''
print(quicksort([3,6,8,10,1,2,1]))

x, y = np.arange(5), np.arange(5)[:, np.newaxis]
distance = np.sqrt(x ** 2 + y ** 2)
plt.pcolor(distance)
plt.colorbar()
plt.show()
print("done")
'''