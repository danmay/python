#! python

# from: http://cmdlinetips.com/2011/08/three-ways-to-read-a-text-file-line-by-line-in-python/

# Make functions
#def my_split(delimiters, string, maxsplit=0):
#    import re
#    regexPattern = '|'.join(map(re.escape, delimiters))
#    return re.split(regexPattern, string, maxsplit)

#def makeFileHEader(fileName):
#	# print to file the header part of the VHDL library
#	f_out = open("eth_data.vhd", "w")
from typing import List, Any


def splitBytes(inString):
	'''
	DOCSTRING: splitBytes: this function will split the input string to HEX bytes
	each char is used as a nibble and two chars are a byte which will be placed in
	the output list.
	in case the string length is not even, zero will be added ad the end!!!
	error will be printed
	all the string MUST be of HEX chars
	'''

	#hexCharList = [0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,a,b,c,d,e]

	if (len(inString) % 2 == 1):
		inStringPad = inString + '0'
		print("ERROR: in splitBytes: {} string can not divided to bytes. Length is not EVEN! inserting ZERO!!!".format(inString))
	else:
		inStringPad = inString

	byteList = []
	for i in range(0,len(inStringPad),2):
		byteList.append(inStringPad[i:i+2])

	return byteList
'''
def buildEthPacket(packetParam):
	packetString = ''
	packetString = packetString + packetParam['eth']['DstAddr']
	packetString = packetString + packetParam['eth']['SrcAddr']
	packetString = packetString + packetParam['eth']['TypeLen']

	## Add the IP data
	if packetParam['typeOfpacket'] == 'IP':
		packetString = packetString +     packetParam['eth']['ethData']['IPv4']['ver']
		packetString = packetString + str(packetParam['eth']['ethData']['IPv4']['HLen'])
		packetString = packetString +     packetParam['eth']['ethData']['IPv4']['TypeSer']
		packetString = packetString +     packetParam['eth']['ethData']['IPv4']['Length']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['FlgOffset']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['TTL']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['Protocol']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['ChSum']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['ChSum']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['srcAddr']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['dstAddr']
		packetString = packetString + 	  packetParam['eth']['ethData']['IPv4']['data']
	else:
		packetString = packetString + packetParam['eth']['ethData']


	return packetString
'''

def getRandomByte():
	import random

	outByte = ""

	randByte = hex(int(255*random.random()))
	outByte = randByte[2:]
	if (len(outByte)<2):
		outByte = '0' + outByte
	#print(outByte)
	return outByte

def buildEthPacketBytes(packetParam):
	packetList: List[Any] = []
	packetList.extend(splitBytes(packetParam['eth']['DstAddr']))
	packetList.extend(splitBytes(packetParam['eth']['SrcAddr']))
	packetList.extend(splitBytes(packetParam['eth']['TypeLen']))

	## Add the IP data
	if packetParam['typeOfpacket'] == 'IP':
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['VerHLen']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['TypeSer']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['Length']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['FlgOffset']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['TTL']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['Protocol']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['ChSum']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['srcAddr']))
		packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['dstAddr']))
		for i in range(200):
			if (packetParam['DataFillType'][0] == 'const'):
				packetList.extend(splitBytes(packetParam['eth']['ethData']['IPv4']['data']))
			elif (packetParam['DataFillType'][0] == 'rnd'):
				packetList.append(getRandomByte())
	else:
		packetList.extend(splitBytes(packetParam['eth']['ethData']))
	#print(packetList)
	return packetList


def packet2file_Header():
	'''
	DOCSTRING: packet2file_Header - this function will produce the VHDL Package file
	the Header section of the file
	'''
	# open the output VHDL file
	#f_out = open('eth_packets.vhd', "w")

	# write the first lines of the package
	f_out.write('library std;  \n')
	f_out.write('use std.textio.all;  \n \n')
	f_out.write('package mii_sim_frame_data_pkg is     \n')
	f_out.write('   constant frame_data : frame_typ_ary := (    \n \n \n')

def packet2file_End():
	'''
	DOCSTRING: packet2file_End - this function will produce the VHDL Package file
	the footer section of the file
	'''
	# open the output VHDL file
	#f_out = open('eth_packets.vhd', "w")

	# write the first lines of the package
	f_out.write('      \n \n \n')
	f_out.write('end package mii_sim_frame_data_pkg;   \n \n \n')
	f_out.write('package body package mii_sim_frame_data_pkg is   \n \n')
	f_out.write('   end;      \n \n \n')



def packet2file(packetNumber, packetByteList):

	#f_out = open('eth_packets.vhd', "w")
	f_out.write("------------------------- \n")
	f_out.write("-- Frame  {} \n".format(packetNumber))
	f_out.write("------------------------- \n")

	f_out.write("{} => (  \n".format(packetNumber))
	f_out.write("	columns => (    \n")
	### output the packet data Byte in each line
	byteNum = 0
	for byte in packetByteList:
		f_out.write("		{} => ( DATA => X\"{}\" , VALID => '1', ERROR => '0'), \n".format(str(byteNum), byte))
		#f_out_sim.write(str(byteNum))
		byteNum = byteNum +1

	f_out.write("		others => ( DATA => X\"{}\" , VALID => '0', ERROR => '0'), \n".format('00'))
	f_out.write("		-- No Error in this frame \n")
	f_out.write("		bad_frame => false), \n \n \n")

	#f_out_sim.write("\n")

def packet2SimFile(packetNumber, packetByteList):
	#byteNum = 0
	packetString = ''
	for byte in packetByteList:
		packetString = packetString + str(byte)
		#f_out_sim.write(str(byte))
		#byteNum = byteNum +1

	f_out_sim.write(" {0} , {0} ,{1}\n".format(str(packetNumber), packetString))


def packet2ExpectedFile(packetNumber, packetByteList):

	### output the packet data Byte in each line
	byteNum = 0
	for byte in packetByteList:
		f_out_sim.write(str(byteNum))
		byteNum = byteNum +1

	f_out_sim.write("\n")


def calFrameSHA(frameString):
	import hashlib

	from hashlib_data import lorem

	h = hashlib.sha1()
	h.update('''Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
sunt in culpa qui officia deserunt mollit anim id est laborum.''')
	print (h.hexdigest)



def readCompareFile(expectedFileName, rcvFileName):
	import csv
	indexDic = {'indexNum': 0, 'indexTime': 1, 'indexData': 2}
	sLow  = 60
	sHigh = 60 + 16

	packDic = {}


	#f_expected = open('eth_packets_sim_in.txt')


	#csvReader = csv.reader(f_expected, delimiter=',')

	with open(expectedFileName) as f_expected:
		csvReader = csv.reader(f_expected, delimiter=',')
		line_count = 0
		for row in csvReader:
			# print(row)
			keyString = row[indexDic['indexData']][sLow:sHigh]
			print(keyString)
			packDic[keyString] = [row[0]]

		print(packDic)

	with open(rcvFileName) as f_rcv:
		csvReader = csv.reader(f_rcv, delimiter=',')
		for row in csvReader:
			keyString = row[indexDic['indexData']][sLow:sHigh]
			if keyString in packDic:
				packDic[keyString].append(row[0])

	print(packDic)


			#if line_count == 0:
			#	print(f'Column names are {", ".join(row)}')
			#	line_count += 1
			#else:
			#	print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
			#	line_count += 1
		#print(f'Processed {line_count} lines.')



#-------------------------
#-- Main Code
#-------------------------

def main():
	import re
	import json
	print('Hello, world!')

	#getRandomByte()
	#print("This is my file to test Python's execution methods.")
	#print("The variable __name__ tells me which context this file is running in.")
	#print("The value of __name__ is:", repr(__name__))


	#calFrameSHA("abcdefghijklmnop")

	f_stream = open('eth_stream.json',"r")
	packetStream = json.load(f_stream)

	streamList = packetStream['streamData']

	# Write the data to file
	f_out = open('eth_packets.vhd', "w")

	f_out_sim = open('eth_packets_sim.txt', "w")

	#f_expected = open('eth_packets_expected.txt', "w")


	#f_in_sim = open('eth_out_packets_sim.txt')



	packet2file_Header()


	packetNumber = 0

	for streamItem in streamList:
		fileName = streamItem[0]+".json"
		numberOfPackets = streamItem[1];
		f_in = open(fileName,"r")
		#f_in = open('eth_packet.json',"r")
		packetParam = json.load(f_in)

		for frame in range(0,numberOfPackets):
			packetByteList = buildEthPacketBytes(packetParam)

			packet2file(packetNumber, packetByteList)
			packet2SimFile(packetNumber, packetByteList)
			#print(packetByteList)
			packetNumber = packetNumber +1
		f_in.close()



	packet2file_End()

	readCompareFile('eth_packets_sim_in.txt', 'eth_packets_sim_in1.txt')


	f_out.close()
	f_out_sim.close()

	print("====== End Script - made {} packets ==========".format(str(packetNumber)))
		

#----------------------
if ()